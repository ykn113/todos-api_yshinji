package com.example

import org.joda.time.DateTime

data class TodoEditParameter (
    val id: Int,
    val title: String,
    val detail: String,
    val date: String,
)