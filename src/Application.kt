package com.example

import com.example.route.todos
import com.fasterxml.jackson.annotation.JsonProperty
import io.ktor.application.*
import io.ktor.routing.*
import com.fasterxml.jackson.databind.*
import io.ktor.jackson.*
import io.ktor.features.*
import io.ktor.response.*
import org.jetbrains.exposed.sql.Database

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

private fun connectDB() {
    Database.connect(
        url = "jdbc:mysql://127.0.0.1/todo",
        driver = "com.mysql.cj.jdbc.Driver",
        user = "root",
        password = "SO1Snowno3NIX"
    )
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    connectDB()

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
    install(StatusPages) {
        exception<SystemException> { cause ->
            call.response.status(cause.status)
            call.respond(cause.response())
        }
    }

    routing {
        todos()
    }
}

data class Todo(
    val id: Int,
    val title: String,
    val detail: String,
    val date: String,
)

data class TodoResponse(
    @JsonProperty("error_code")
    override val errorCode: Int = 0,
    @JsonProperty("error_message")
    override val errorMessage: String = ""
) : BaseResponse

data class TodosResponse(
    val todos: List<Todo>,
    @JsonProperty("error_code")
    override val errorCode: Int = 0,
    @JsonProperty("error_message")
    override val errorMessage: String = ""
) : BaseResponse
