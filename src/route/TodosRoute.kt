package com.example.route

import com.example.*
import com.example.repository.ErrorCode.Companion.DeleteErrorCode
import com.example.repository.ErrorCode.Companion.GetErrorCode
import com.example.repository.ErrorCode.Companion.PostErrorCode
import com.example.repository.ErrorCode.Companion.PutErrorCode
import com.example.repository.ErrorMessage.Companion.DeleteErrorMessage
import com.example.repository.ErrorMessage.Companion.GetErrorMessage
import com.example.repository.ErrorMessage.Companion.PostErrorMessage
import com.example.repository.ErrorMessage.Companion.PutErrorMessage
import com.example.repository.TodoRepository
import com.fasterxml.jackson.core.JsonParseException
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.todos() {
    val todoRepository = TodoRepository()

    route("/todos") {
        get {
            try {
                val todos = todoRepository.findAll()
                call.respond(HttpStatusCode.OK, TodosResponse(todos))
                //call.respond(todoRepository.findAll())
            } catch (e: Exception) {
                when (e) {
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(GetErrorMessage, GetErrorCode)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        post {
            try {
                //val newTodo = call.receive<TodoCreateParameter>()
                //todoRepository.create(newTodo)
                val parameter = call.receive<TodoCreateParameter>()
                todoRepository.create(parameter)
                call.respond(HttpStatusCode.OK, TodoResponse())
            } catch (e:Exception) {
                when (e) {
                    is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(PostErrorMessage, PostErrorCode)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        put ("/{id}"){
            try {
                val id = call.parameters["id"]!!.toInt()
                val parameter = call.receive<TodoEditParameter>()
                todoRepository.edit(id, parameter)
                call.respond(HttpStatusCode.OK, TodoResponse())
            } catch (e: Exception) {
                when (e) {
                    is NullPointerException, is JsonParseException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(PutErrorMessage, PutErrorCode)
                    }
                    else -> throw UnknownException()
                }
            }
        }

        delete("/{id}") {
            try {
                val id = call.parameters["id"]?.toInt() ?: return@delete
                todoRepository.delete(id)
                call.respond(HttpStatusCode.OK, TodoResponse())
            } catch (e: Exception) {
                when (e) {
                    is NullPointerException -> {
                        throw BadRequestException()
                    }
                    is RecordInvalidException -> {
                        throw InternalServerErrorException(DeleteErrorMessage, DeleteErrorCode)
                    }
                    else -> throw UnknownException()
                }
            }
        }
    }
}