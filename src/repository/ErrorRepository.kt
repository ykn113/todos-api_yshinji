package com.example.repository

import com.example.BaseResponse
import com.fasterxml.jackson.annotation.JsonProperty

data class ErrorResponse(
    @JsonProperty("error_code")
    override val errorCode: Int,
    @JsonProperty("error_message")
    override val errorMessage: String
) : BaseResponse

class ErrorCode {
    companion object {
        const val UnknownErrorCode = 1
        const val BadRequestErrorCode = 2
        const val GetErrorCode = 3
        const val PostErrorCode = 4
        const val PutErrorCode = 5
        const val DeleteErrorCode = 6
    }
}
class ErrorMessage {
    companion object {
        const val UnknownErrorMessage = "サーバー内で不明なエラーが発生しました。"
        const val BadRequestErrorMessage = "リクエストの形式が不正です。"
        const val GetErrorMessage = "一覧の取得に失敗しました。"
        const val PostErrorMessage = "登録に失敗しました。"
        const val PutErrorMessage = "更新に失敗しました。"
        const val DeleteErrorMessage = "削除に失敗しました。"
    }
}
