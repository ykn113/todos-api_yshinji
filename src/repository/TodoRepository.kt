package com.example.repository

import com.example.*
import com.example.db.table.Todos
//import com.example.repository.ErrorCode.Companion.GetErrorCode
//import com.example.repository.ErrorMessage.Companion.GetErrorMessage
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.update
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

class TodoRepository {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd")
    fun findAll(): List<Todo> {
        try {
            return transaction {
                Todos.selectAll().sortedBy{(it[Todos.date])}.map {
                    Todo(
                        id = it[Todos.id].value,
                        title = it[Todos.title].toString(),
                        detail = it[Todos.detail].toString(),
                        date = formatter.print(it[Todos.date]).toString(),
                    )
                }
            }
        } catch (e: Throwable) {
            when (e) {
                is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }

    fun create(parameter: TodoCreateParameter) {
        try {
            transaction {
                Todos.insert {
                    it[title] = parameter.title
                    it[detail] = parameter.detail
                    it[date] = DateTime.parse(parameter.date, formatter)
                    it[created_at] = DateTime.now()
                    it[updated_at] = DateTime.now()
                }
            }
        } catch (e: Throwable) {
            when (e) {
                is IllegalArgumentException, is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }

    fun edit(id: Int, parameter: TodoEditParameter) {
        try {
            transaction {
                val isFailed = Todos.update({ Todos.id eq id }) {
                    it[title] = parameter.title
                    it[detail] = parameter.detail
                    it[date] = DateTime.parse(parameter.date, formatter)
                    it[updated_at] = DateTime.now()
                }== 0
                if (isFailed) throw RecordInvalidException()
            }
        } catch (e: Throwable) {
            when (e) {
                is IllegalArgumentException, is RecordInvalidException, is ExposedSQLException -> {
                    throw RecordInvalidException()
                }
                else -> throw e
            }
        }
    }

    fun delete(id: Int) {
        try {
            transaction{
                val isFailed = Todos.deleteWhere { Todos.id eq id } == 0
                if (isFailed) throw RecordInvalidException()
            }
        } catch (e: Throwable) {
            when (e) {
                is RecordInvalidException, is ExposedSQLException -> throw RecordInvalidException()
                else -> throw e
            }
        }
    }
}