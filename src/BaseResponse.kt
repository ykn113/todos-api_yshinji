package com.example

interface BaseResponse {
    val errorCode: Int
    val errorMessage: String
}