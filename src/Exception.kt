package com.example

import com.example.repository.ErrorCode.Companion.BadRequestErrorCode
import com.example.repository.ErrorCode.Companion.UnknownErrorCode
import com.example.repository.ErrorMessage.Companion.BadRequestErrorMessage
import com.example.repository.ErrorMessage.Companion.UnknownErrorMessage
import com.example.repository.ErrorResponse
import io.ktor.http.*

abstract class SystemException(message: String, private val code: Int? = null, ex: Exception? = null) :
    RuntimeException(message, ex) {
    abstract val status: HttpStatusCode
    fun response() = ErrorResponse(
        errorCode = code ?: status.value,
        errorMessage = message ?: "error"
    )
}
class UnknownException : SystemException {
    constructor(message: String = UnknownErrorMessage) : super(message, code = UnknownErrorCode)

    override val status: HttpStatusCode = HttpStatusCode.InternalServerError
}

class InternalServerErrorException : SystemException {
    constructor(message: String, code: Int) : super(message, code)

    override val status: HttpStatusCode = HttpStatusCode.InternalServerError
}

class BadRequestException : SystemException {
    constructor(message: String = BadRequestErrorMessage) : super(message, code = BadRequestErrorCode)

    override val status: HttpStatusCode = HttpStatusCode.BadRequest
}

class RecordInvalidException : Exception()
