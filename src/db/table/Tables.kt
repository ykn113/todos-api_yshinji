package com.example.db.table

import org.jetbrains.exposed.dao.IntIdTable
import org.jetbrains.exposed.sql.CurrentDateTime

object Todos : IntIdTable() {
    val title = varchar("title", 100)
    val detail = varchar("detail", 1000).nullable()
    val date = date("date").nullable()
    val created_at = datetime("created_at").defaultExpression(CurrentDateTime())
    val updated_at = datetime("updated_at").defaultExpression(CurrentDateTime())
}

