package com.example

import com.example.repository.TodoRepository
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.junit.Test
import kotlin.test.assertEquals


class TodoRepositoryTest {
    @Test
    fun testGetSuccess(){
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/todos").apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    fun testGetError(){
        withTestApplication(Application::module) {
            handleRequest(HttpMethod.Get, "/test").apply {
                assertEquals(HttpStatusCode.OK, response.status())
            }
        }
    }

    @Test
    fun testCreateSuccess(){
        val str: String = "{\"title\":\"title\",\"detail\":\"detail\",\"date\":\"2022-01-19\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }
    @Test
    fun testCreateError(){
        val str: String = "{\"title\":\"title\",\"detail\":\"detail\",\"date\":\"アイウエオ\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Post,"/todos"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    @Test
    fun testPutSuccess(){
//    データは更新されるけどエラーになる
        val str: String = "{\"id\":4,\"title\":\"test1\",\"detail\":\"test1\",\"date\":\"2022-01-19\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/4"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }
    @Test
    fun testPutErrorRequest(){
        val str: String = "{\"id\":2,\"title\":\"title\",\"detail\":\"detail\",\"date\":\"アイウエオ\"}"
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Put,"/todos/2"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
                setBody(str)
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }

    @Test
    fun testDeleteSuccessRequest(){
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/3"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }
    @Test
    fun testDeleteErrorRequest(){
        withTestApplication(Application::module){
            handleRequest(HttpMethod.Delete,"/todos/aaa"){
                addHeader(HttpHeaders.ContentType,ContentType.Application.Json.toString())
            }.run {
                assertEquals(HttpStatusCode.OK,response.status())
            }
        }
    }
}

